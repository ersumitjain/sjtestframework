Pod::Spec.new do |s|
          #1.
          s.name               = "MyTestFramework"
          #2.
          s.version            = "1.0.0"
          #3.  
          s.summary         = " This is use to customise UIVIew 'MyTestFramework' framework"
	#4.
          s.homepage        = "https://ersumitjain@bitbucket.org/ersumitjain/sjtestframework.git"
          #5.
          s.license              = "MIT"
          #6.
          s.author               = "Sumit Jain"
          #7.
          s.platform            = :ios, "11.0"
          #8.
          s.source              = { :git => "https://ersumitjain@bitbucket.org/ersumitjain/sjtestframework.git", :tag => "1.0.0" }
          #9.
          s.source_files     = "MyTestFramework", "MyTestFramework/**/*.{h,m,swift}"
    end